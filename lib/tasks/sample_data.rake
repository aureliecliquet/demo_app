namespace :db do
  desc 'Fill database with sample data'
  task populate: :environment do
    require 'faker'
    # Faker::Config.locale = :fr
    include ActionView::Helpers::DateHelper
    Rake::Task['db:reset'].invoke

    populate_authors
    populate_books
  end
end

def populate_books
  800.times do |_n|
    name       = Faker::Book.title
    author_id  = rand(1..100)
    isbn       = Faker::Code.isbn(13)
    Book.create!(name: name,
                 author_id: author_id,
                 isbn: isbn)
  end
end

def populate_authors
  100.times do |_n|
    name       = Faker::Book.author
    state      = Faker::Address.state

    Author.create!(name: name,
                   state: state)
  end
end
