json.extract! author, :id, :name, :state, :created_at, :updated_at
json.url author_url(author, format: :json)