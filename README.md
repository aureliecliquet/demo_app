README
==

Clone project
--

```
git clone git@github.com:fusco/demo_app.git
```


Install :
--

```
cd demo_app

bundle install

rake db:create

rake db:migrate

rake db:populate

```


Play
--

```
rails c
```
